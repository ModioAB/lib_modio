# vim: set fileencoding=utf-8 ts=4 sts=4 sw=4 ft=python expandtab :
import unittest

from modio.ipc import (
    validate_key,
    validate_transaction_token,
)


class TestValidKey(unittest.TestCase):
    def test_valid_key_ok(self):
        validate_key("foo.bar")
        validate_key("foo.bar.12.1")
        validate_key("foo-bar.12.1")
        validate_key("foo_bar.12.1")

    def test_key_contains_invalid_chars(self):
        with self.assertRaises(ValueError):
            validate_key("foo,bar")
        with self.assertRaises(ValueError):
            validate_key("foo\\bar")
        with self.assertRaises(ValueError):
            validate_key("foo./bar")

    def test_key_too_short(self):
        with self.assertRaises(ValueError):
            validate_key("")

        with self.assertRaises(ValueError):
            validate_key("a")

    def test_key_has_misplaced_periods(self):
        with self.assertRaises(ValueError):
            validate_key(".")
        with self.assertRaises(ValueError):
            validate_key(".foo")
        with self.assertRaises(ValueError):
            validate_key("foo.")
        with self.assertRaises(ValueError):
            validate_key("foo..bar")

    def test_validate_key_invalid_type(self):
        from decimal import Decimal

        with self.assertRaises(TypeError):
            validate_key(None)
        with self.assertRaises(TypeError):
            validate_key([])
        with self.assertRaises(TypeError):
            validate_key(11)
        with self.assertRaises(TypeError):
            validate_key(Decimal("12"))

    def test_valid_token(self):
        import uuid

        validate_transaction_token("1449838461")
        validate_transaction_token(1449838461)
        validate_transaction_token(uuid.uuid1())
        validate_transaction_token(str(uuid.uuid1()))

    def test_invalid_token(self):
        with self.assertRaises(ValueError):
            validate_transaction_token(";.;")
        with self.assertRaises(ValueError):
            validate_transaction_token("\n.")
        with self.assertRaises(ValueError):
            validate_transaction_token("åäö")
        with self.assertRaises(ValueError):
            validate_transaction_token("_12_12")
        with self.assertRaises(ValueError):
            validate_transaction_token(None)
        with self.assertRaises(ValueError):
            validate_transaction_token("")


if __name__ == "__main__":
    unittest.main()
