# vim: ts=4 sts=4 sw=4 ft=python expandtab :
# SPDX-License-Identifier: MIT
"""Decrypting config keys helpers."""
import base64
import subprocess

SSLKEY = "/modio/mytemp.key"


def decrypt(secret: str, keyfile: str = SSLKEY) -> str:
    """Decrypts a secret.

    secret is usually from from a config file or similar.
    keyfile defaults to the host private key.
    """
    coded = secret.encode("utf-8")
    encrypted = base64.b64decode(coded)
    decrypted = subprocess.check_output(
        ["openssl", "rsautl", "-decrypt", "-inkey", keyfile],
        input=encrypted,
    )
    return decrypted.decode()
