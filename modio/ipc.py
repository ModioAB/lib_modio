# vim: ts=4 sts=4 sw=4 ft=python expandtab :
# SPDX-License-Identifier: MIT
"""IPC /logger related helpers."""
import re
import time
import uuid
import logging
from decimal import Decimal

from typing import Union, Optional, Any, List, Tuple, Dict
import dbus

INVALID_KEY = re.compile(r"[^A-Za-z0-9_\-.]")

Token = Union[str, int, uuid.UUID]
ValueMap = Dict[int, str]
DataBatch = Dict[str, Union[int, float, str, bool, Decimal]]

log = logging.getLogger(__name__)


class NoData(Exception):
    """No data from the service."""


def validate_key(key: str) -> None:
    """Internal: Validate a key content against Zabbix rules."""
    if INVALID_KEY.search(key) is not None:
        raise ValueError("Not a valid key format")

    if ".." in key:
        raise ValueError(".. is not allowed in a key")

    if key.startswith(".") or key.endswith("."):
        raise ValueError("Key cannot start or end with period")

    if len(key) < 2:
        raise ValueError("Too short key")

    if len(key) > 80:
        raise ValueError("Too long key")


def validate_transaction_token(token: Token) -> None:
    """Validate a transaction token is legal."""
    try:
        token = str(token)
    except ValueError as ex:
        raise ValueError("token must be stringable") from ex

    # Any integer is valid
    try:
        int(token)
    except ValueError:
        try:
            uuid.UUID(token)
        except Exception as ex:
            raise ValueError("token must be integer or UUID") from ex


Point = Tuple[str, Any, int]


class FsIPC:
    """Wrapper around the dbus Service."""

    def __init__(self, session: bool = False):
        """Connect on dbus and get an ipc."""
        if session:
            bus = dbus.SessionBus()
        else:
            bus = dbus.SystemBus()
        try:
            modio_logger = bus.get_object(
                "se.modio.logger", "/se/modio/logger"
            )
        except Exception as exc:
            # pylint: disable=W0719
            raise Exception(f"Logger is not running on bus: {bus}") from exc
        self._bus = bus
        self._fsipc = dbus.Interface(modio_logger, "se.modio.logger.fsipc")
        self._logger1 = dbus.Interface(
            modio_logger, "se.modio.logger.Logger1"
        )
        log.debug("Got bus: %s  and ipc: %r", bus, self._fsipc)

    def store(self, key: str, value: Any) -> None:
        """Store a single (key, value)."""
        value = str(value)
        self._fsipc.Store(key, value)

    def store_batch(self, batch: DataBatch) -> None:
        """Store a batch of (key, value) using logger side timestamp."""
        # We need to type-convert Decimal to something as it does not serialize
        # on the wire.
        # Here, we do it to str as that is closest to what we did before.
        for key, value in batch.items():
            if isinstance(value, Decimal):
                batch[key] = str(value)
        self._logger1.StoreBatch(batch)

    def store_buffered(self, key: str, arr: List[Tuple[Any, Any]]) -> None:
        """Store a buffer of (value, timestamp)."""
        # dbus has stricter semantics about array contents than Python does
        results = []
        for val, tim in arr:
            val = str(val)
            tim = int(tim)
            res = self._fsipc.StoreWithTime(key, val, tim)
            results.append(res)

    def retrieve(self, key: str, max_age: Optional[float] = None) -> Point:
        """Return a point for an key."""
        errors = (
            "org.freedesktop.DBus.Python.KeyError",
            "org.freedesktop.DBus.Python.modio.ipc.NoData",
            "se.modio.logger.fsipc.NotFound",
        )
        try:
            to_cast = self._fsipc.Retrieve(key)
        except dbus.exceptions.DBusException as ex:
            if ex.get_dbus_name() in errors:
                raise NoData() from ex
            raise ex
        if not to_cast:
            raise NoData()
        point = (str(to_cast[0]), to_cast[1], int(to_cast[2]))

        if max_age is None:
            return point

        # Implement max_age for compatibility with old FsIPC
        now = time.time()
        _, _, tim = point
        if abs(now - int(tim)) < max_age:
            return point
        raise NoData("No recent data")

    def store_transaction(
        self, key: str, old: Any, new: Any, token: Optional[Any] = None
    ) -> None:
        """Store a transaction in the system."""
        if token is None:
            token = uuid.uuid4()

        # dbus has stricter requirements, stringify first, duck-typing on dbus
        old = str(old)
        new = str(new)
        token = str(token)
        self._fsipc.TransactionAdd(key, old, new, token)

    def transaction_pass(self, t_id: Any) -> None:
        """Pass a transaction for the transaction id t_id."""
        t_id = int(t_id)
        self._fsipc.TransactionPass(t_id)

    def transaction_fail(self, t_id: Any) -> None:
        """Fail a transaction for the transaction id t_id."""
        t_id = int(t_id)
        self._fsipc.TransactionFail(t_id)

    def transactions(self, keyspace: str) -> Any:
        """Get all transactions for the matching keyspace.

        Keyspace should be a valid key prefix, like "modbus.foobar."
        """
        return self._fsipc.TransactionGet(keyspace)


class UnitError(Exception):
    """Perhaps you tried to replace the unit."""


class UnitUnknown(UnitError):
    """Unknown Unit.

    Not SenML? See whitelist
    https://gitlab.com/ModioAB/modio-logger/-/blob/main/modio-logger/src/types.rs
    """


class Metadata:
    """Wrapper around the dbus Logger1 Metadata service."""

    def __init__(self, session: bool = False) -> None:
        """Connect on dbus and get an ipc."""
        if session:
            bus = dbus.SessionBus()
        else:
            bus = dbus.SystemBus()
        try:
            service = bus.get_object("se.modio.logger", "/se/modio/logger")
        except Exception as exc:
            # pylint: disable=W0719
            raise Exception(
                f"Metadata capable Logger is not running on bus: {bus}"
            ) from exc
        self._log1 = dbus.Interface(service, "se.modio.logger.Logger1")
        log.debug("Got bus: %s  and Logger1: %r", bus, self._log1)

    def set_name(self, key: str, name: str) -> None:
        """Set the name for the key."""
        self._log1.SetMetadataName(key, name)

    def set_description(self, key: str, description: str) -> None:
        """Set the description for the key."""
        self._log1.SetMetadataDescription(key, description)

    def set_mode(self, key: str, mode: int) -> None:
        """Set the mode (read/write) for the key."""
        self._log1.SetMetadataMode(key, mode)

    def set_unit(self, key: str, unit: str) -> None:
        """Set the unit for the key."""
        if not unit:
            # Empty unit  "" is not valid according to our system.
            return
        try:
            self._log1.SetMetadataUnit(key, unit)
        except dbus.exceptions.DBusException as exc:
            # This raises an error if one either:
            #  - replaces a unit with another
            #  - feeds it an unrecognized unit
            err_msg = exc.get_dbus_message()
            log.error(
                "Failed to set unit, key: '%s', unit: '%s', error: '%s'",
                key,
                unit,
                err_msg,
            )
            if err_msg == "Unknown unit":
                raise UnitUnknown(f"{err_msg} {unit}") from exc

            if err_msg == "May not replace unit":
                raise UnitError(err_msg) from exc

            raise exc

    def set_value_map(self, key: str, value_map: ValueMap) -> None:
        """Set the unit for the key."""
        self._log1.SetMetadataValueMap(key, value_map)

    def push_senml_dict(
        # pylint: disable=too-many-arguments
        self,
        *,
        n: str,  # pylint: disable=invalid-name
        name: Optional[str] = None,
        u: Optional[str] = None,  # pylint: disable=invalid-name
        description: Optional[str] = None,
        value_map: Optional[ValueMap] = None,
        mode: Optional[int] = None,
    ) -> None:
        """Take a dict in senml format and tries to push it.

        Swallows errors around unit handling. If you want to see that the unit
        is accepted, use the "set_unit" method instead.
        """
        key = n
        if ":" in key:
            _, key = key.split(":")
        if name:
            self.set_name(key, name)
        if u:
            unit = u.strip()
            try:
                self.set_unit(key, unit)
            except UnitError:
                pass

        if description:
            self.set_description(key, description)

        if mode is not None:
            self.set_mode(key, mode)

        if value_map:
            self.set_value_map(key, value_map)
