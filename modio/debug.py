# vim: ts=4 sts=4 sw=4 ft=python expandtab :
# SPDX-License-Identifier: MIT
"""Stack and signal exception helpers."""
import signal
import sys
import threading
import traceback

from typing import Any
from typing import Callable
from typing import Optional
from typing import Union
from types import FrameType


SigHandler = Union[Callable[[int, Optional[FrameType]], Any], int, None]


def dumpstacks(_signal: Any, _frame: Any) -> None:
    """Dump current stacks."""
    id2name = {th.ident: th.name for th in threading.enumerate()}
    code = []
    current_frames = sys._current_frames()  # pylint: disable=protected-access
    for thread_id, stack in current_frames.items():
        code.append(f"\n# Thread: {id2name[thread_id]}({thread_id})")
        for filename, lineno, name, line in traceback.extract_stack(stack):
            if name == "stacktrace":
                # Skip the signal handler frames
                break
            code.append(f'  File: "{filename}", line {lineno}, in {name}')
            if line:
                code.append("    {line.strip()}")
    print("\n".join(code), file=sys.stderr)


def stacktrace_on_sigusr1() -> SigHandler:
    """Dump the stacks on signal."""
    # No blank lines allowed after function docstring

    def stacktrace(sig: int, frame: Optional[FrameType]) -> None:
        """Dump a stacktrace."""
        dumpstacks(sig, frame)

    return signal.signal(signal.SIGUSR1, stacktrace)
