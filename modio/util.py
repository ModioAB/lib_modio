# vim: ts=4 sts=4 sw=4 ft=python expandtab :
# SPDX-License-Identifier: MIT
"""Misc utils we have."""

import contextlib
import logging
from time import monotonic
from typing import Any
from typing import Callable
from typing import Generator
from typing import Optional
import uuid


def hexme(some: Any) -> str:
    """Turn a sequence to hex, or repr."""
    try:
        if len(some) > 0:
            return " ".join(f"{c:>02X}" for c in some)
    except Exception:  # pylint: disable=broad-except
        pass
    return repr(some)


# For our context manager
EmptyGen = Generator[None, None, None]


@contextlib.contextmanager
def temp_setattr(obj: Any, attr: str, val: Any) -> EmptyGen:
    """Context that changes an attribute while used."""
    oldval = getattr(obj, attr)
    setattr(obj, attr, val)
    try:
        yield
    finally:
        setattr(obj, attr, oldval)


def verbosity(logger: logging.Logger, verbose: int = 3) -> logging.Logger:
    """Turn a -v flag count and set a log-level."""
    if verbose >= 2:
        logger.setLevel(logging.DEBUG)
    elif verbose >= 1:
        logger.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.WARNING)
    return logger


def get_mac() -> str:
    """Get mac/address from /sys/.."""
    with open("/sys/class/net/wan/address", "r", encoding="utf-8") as fobj:
        return fobj.read().strip().replace(":", "")


def get_host() -> str:
    """Get a mac ddress from /sys or fallback to uuidd.

    uuidd returns the wrong data on real hardware.
    """
    try:
        return get_mac()
    except OSError:
        # uuid.getnode returns random data on devices.
        node = uuid.getnode()
        return f"{node:012x}"


class Timer:
    """Timing context manager."""

    def __init__(self, timefn: Callable[..., float] = monotonic) -> None:
        """Set up the timing manager."""
        self.timefn = timefn
        self.start: Optional[float] = None
        self.stop: Optional[float] = None

    def __enter__(self) -> "Timer":
        """Start measuring."""
        if self.start is not None:
            raise ValueError("Timer already started.")
        self.start = self.timefn()
        return self  # Enables `with Timer() as t:` use.

    # pylint: disable=useless-return
    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any) -> None:
        """End measuring."""
        self.stop = self.timefn()
        # Returning truthy suppresses exceptions. We dont want that
        return None

    @property
    def elapsed_time(self) -> float:
        """Show elapsed time."""
        if self.start is None:
            return 0.0
        if self.stop is None:
            return self.timefn() - self.start
        return self.stop - self.start
