# vim: ts=4 sts=4 sw=4 ft=python expandtab :
# SPDX-License-Identifier: MIT
"""Iterator utilitites."""

import random
import time
from itertools import zip_longest
from typing import Any
from typing import Callable
from typing import Iterable
from typing import Iterator
from typing import Optional
from typing import Union

Number = Union[int, float]


def chunked(
    iterable: Iterable[Any], size: int, shuffle: bool = False
) -> Iterable[Any]:
    """
    Pass over an iterable in chunks of up to size values, padding with None.

    Can shuffle the input list for convenience
    """
    seq = list(iterable)
    if shuffle:
        random.shuffle(seq)
    iterables = [iter(seq)] * size
    return zip_longest(*iterables, fillvalue=None)


class periodical_cycles:  # pylint: disable=invalid-name
    """Periodic cycle generator."""

    def __init__(
        self,
        period: Number,
        cycles: Optional[int] = None,
        start_delay: Optional[Number] = None,
        end_delay: Optional[Number] = None,
    ) -> None:
        """Periodic loop generator.

        Attempts to yield at intervals of 'period' time units.
        If 'cycles' is None, run forever. Otherwise do that many cycles.
        If 'start_delay' is not None, wait that long before first yield.
        Wait 'end_delay' time units before exiting if 'end_delay' > 0,
        or 'period' time units if 'end_delay' is None.
        """
        self.period = period
        self.cycles = cycles
        self.start_delay = start_delay or 0
        self.end_delay = end_delay
        self._next: Optional[float] = None

    @property
    def nextstart(self) -> Optional[Number]:
        """Read to see when we are supposed to run next."""
        return self._next

    def __iter__(
        self,
        now: Callable[..., float] = time.monotonic,
        wait: Callable[..., None] = time.sleep,
    ) -> Iterator[int]:
        """Loop over to yield every period in time.

        wait is a callable to sleep.
        now is a callable to get current time.
        """
        period = self.period
        cycles = self.cycles
        start_delay = self.start_delay
        end_delay = self.end_delay
        self._next = _next = now() + start_delay
        if end_delay is None:
            end_delay = period
        cycle = 0
        while cycles is None or cycle < cycles:
            while now() < _next:
                # Lower bound on time to wait.
                wait(max(_next - now(), 0))
            if now() - _next > period / 2:
                self._next = _next = now()
            self._next = _next = _next + period
            yield cycle
            cycle += 1
        if end_delay > 0:
            # Adjust for time already passed, bound to [0, end_delay].
            spent = now() - (_next - period)
            end_delay = min(end_delay, max(0, end_delay - spent))
            wait(end_delay)
