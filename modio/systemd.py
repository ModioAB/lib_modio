# vim: ts=4 sts=4 sw=4 ft=python expandtab :
# SPDX-License-Identifier: MIT
"""Functions to work with systemd watchdog."""
import os
import socket

from typing import Tuple
from typing import Optional


def watchdog_socket(
    clean_environment: bool = True,
) -> Tuple[Optional[str], Optional[socket.socket]]:
    """Return the watchdog socket from environment.

    clean_environment removes the varaibles from env to prevent children
    from inheriting it and doing something wrong.
    """
    _empty = None, None
    address = os.environ.get("NOTIFY_SOCKET", None)
    if clean_environment:
        address = os.environ.pop("NOTIFY_SOCKET", None)

    if not address:
        return _empty

    if len(address) == 1:
        return _empty

    if address[0] not in ("@", "/"):
        return _empty

    if address[0] == "@":
        address = "\0" + address[1:]

    # SOCK_CLOEXEC was added in Python 3.2 and requires Linux >= 2.6.27.
    # It means "close this socket after fork/exec()
    try:
        sock = socket.socket(
            socket.AF_UNIX, socket.SOCK_DGRAM | socket.SOCK_CLOEXEC
        )
    except AttributeError:
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)

    return address, sock


def watchdog_ping(
    address: Optional[str], sock: Optional[socket.socket]
) -> bool:
    """Ping the watchdog."""
    message = b"WATCHDOG=1"
    if not (address and sock):
        return False
    try:
        retval = sock.sendto(message, address)
    except socket.error:
        return False

    return retval > 0
